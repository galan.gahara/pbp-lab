from django.shortcuts import render
from django.http import JsonResponse
from lab_2.models import Note

# Create your views here.
def index(request):
    notes = Note.objects.all().values() 
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

def get_note(request):
    context ={}
 
    context["note"] = Note.objects.get(id = id)   

    return render(request, "lab5_index.html", context)

def update_note(request):
    context ={}
 
    obj = get_object_or_404(GeeksModel, id = id)
 
    if request.method =="POST":
        obj.delete()
        return "status 200"
 
    return render(request, "lab5_index.html", context)

def delete_note(request, id):
    context ={}
 
    obj = get_object_or_404(GeeksModel, id = id)
 
    if request.method =="POST":
        obj.delete()
        return JsonResponse(status = 200 )
 
    return render(request, "lab5_index.html", context)