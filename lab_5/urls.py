from django.urls import path
from .views import index,delete_note,update_note,get_note

urlpatterns = [    
    path('', index, name='index'),
    path('notes/<id>/delete', delete_note ),
    path('notes/<id>/update', update_note ),
    path('notes/<id>', get_note ),
]
