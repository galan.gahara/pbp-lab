from .views import index,add_note,note_list
from django.urls import path


urlpatterns = [    
    path('index', index, name='index'),
    path('add_note', add_note, name='add_node'),
    path('note_list', note_list, name='note_list'),
]