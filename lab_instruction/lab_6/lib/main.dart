import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/widgets/main_drawer.dart';

import './dummy_data.dart';
import './screens/tabs_screen.dart';
import './screens/meal_detail_screen.dart';
import './screens/category_meals_screen.dart';
import './screens/filters_screen.dart';
import './screens/categories_screen.dart';
import './models/meal.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const appName = 'HomeMed';

    return MaterialApp(
      title: appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Colors.indigo,
        canvasColor: Color.fromRGBO(255, 255, 255, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color.fromRGBO(0, 0, 0, 1),
            ),
            bodyText2: TextStyle(
              color: Color.fromRGBO(0, 0, 0, 1),
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            )),
      ),
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  List<Map<String, Object>> _pages;
  int _selectedPageIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        Image.network(
            'https://images.everydayhealth.com/homepage/health-topics-2.jpg?sfvrsn=757370ae_2',
            width: 800,
            height: 250,
            fit: BoxFit.cover),
        Positioned(
          // The Positioned widget is used to position the text inside the Stack widget
          bottom: 80,
          right: 100,

          child: Container(
            // We use this Container to create a black box that wraps the white text so that the user can read the text even when the image is white
            width: 300,
            padding: EdgeInsets.all(10),
            child: Text(
              'Hello',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 60,
                color: Colors.white,
                shadows: <Shadow>[
                  Shadow(
                    offset: Offset(10.0, 10.0),
                    blurRadius: 3.0,
                    color: Color.fromARGB(255, 0, 0, 0),
                  ),
                  Shadow(
                    offset: Offset(10.0, 10.0),
                    blurRadius: 8.0,
                    color: Color.fromARGB(125, 0, 0, 255),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    ));
  }
}
