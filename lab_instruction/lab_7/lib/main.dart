import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Form Kerja Bareng",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool checkbox1 = false;
  bool nilaiSwitch = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Form Kerja Bareng"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(9.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "contoh: something@gmail.com",
                      labelText: "Masukan email",
                      icon: Icon(Icons.email),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    maxLines: null,
                    decoration: new InputDecoration(
                      labelText:
                          "Masukan alasan anda ingin bekerja sama dengan kami",
                      icon: Icon(Icons.move_to_inbox),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Alasan tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Row(children: [
                  SizedBox(
                    width: 10,
                    child: Checkbox(
                      value: checkbox1,
                      activeColor: Colors.orange,
                      onChanged: (value) {
                        //value may be true or false
                        setState(() {
                          checkbox1 = !checkbox1;
                        });
                      },
                    ),
                  ),
                  SizedBox(width: 10.0),
                  Text('I accept the Term and Conditions')
                ]),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {}
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
