1. Apakah perbedaan antara JSON dan XML?
JSON atau biasa disebut sebagai JavaScript Object Notation adalah suatu meta-bahasa yang membantu dalam melakukan pertukaran informasi di antara platform-platform yang berbeda. Sedangkan XML atau biasa disebut sebagai eXtensible Markup Language adalah markup language yang biasa dipergunakan untuk menyimpan suatu informasi. 

Perbedaan antara JSON dan XML bisa dilihat di bawah ini :

                JSON                                            
- JSON merupakan suatu bahasa meta    
- Lebih mudah digunakan dan lebih mudah untuk dibaca oleh kaum awam
- Extensi file dari JSON adalah .json
- Didukung banyak Ajax Toolking
- Tidak mendukung penggunaan comments
- Hanya didukung oleh UTF-8 encoding

                 XML
- XML merupakan suatu markup language
- Agak sulit untuk dipahami oleh masyarakat
- Extensi file dari XML adalah .xml
- Tidak didukung oleh seluruh Ajax Toolking
- Mendukung dalam penggunaan comments
- Didukung oleh berbagai jenis encoding


2. Apakah perbedaan antara HTML dan XML?
HTML atau biasa disebut dengan Hypertext Markup Language merupakan suatu bahasa pemrograman untuk menampilkan data serta memberikan deskripsi dari struktur web page's.Sedangkan XML atau biasa disebut sebagai eXtensible Markup Language adalah markup language yang biasa dipergunakan untuk menyimpan suatu informasi. 

Perbedaan antara HTML dan XML bisa dilihat di bawah ini :
                 HTML
- Merupakan Case insensitive
- Dipergunakan untuk menampilkan suatu informasi yang ada
- Penggunaan closing tags tidak terlalu dihiraukan
- Bersifat Static
- Tag HTML sangat terbatas dalam mengembangkannya
- HTML tidak mendukung adanya penggunaan namespaces

                 XML
- Case sensitive
- Dipergunakan untuk menyimpan suatu informasi dan mengirimkan informasi yang ada
- Wajib menggunakan closing tags
- Lebih bersifat Dynamic
- Tag XML bisa dikembangkan menjadi lebih baik
- XML mendukung adanya penggunaan namespaces


